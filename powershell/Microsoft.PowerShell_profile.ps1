# Paths
$env:Path += ";C:\toolz\scripts"
$env:Path += ";C:\toolz\bin"

# Aliases
Set-Alias vim nvim
Set-Alias myip Get-Ip.ps1
Set-Alias alpha Get-Alpha.ps1

# Functions
function prompt {
    #region COLORS
    	$ESC = [char]27
    	$RESET_COLOR = "${ESC}[0m"
        # I've experinced some minor sluggishnes when all the colors is
        # defined. Optimal solution would probably be to load all these
        # values in $ENV on first startup or something similar..

    	$ANSIColors = [PSCustomObject]@{
    	    Red = "${ESC}[31m";           Red_bg = "${ESC}[41m"
    	    Yellow = "${ESC}[33m";        Yellow_bg = "${ESC}[43m"
    	    Blue = "${ESC}[34m";          Blue_bg = "${ESC}[44m"
    	    Cyan = "${ESC}[36m";          Cyan_bg = "${ESC}[46m"
    	    #Magenta = "${ESC}[35m";       Magenta_bg = "${ESC}[45m"
    	    #Green = "${ESC}[32m";         Green_bg = "${ESC}[42m"
    	    # Black = "${ESC}[30m";         Black_bg = "${ESC}[40m"
    	    #White = "${ESC}[37m";         White_bg = "${ESC}[47m"
    	    #BrightBlack = "${ESC}[90m";   BrightBlack_bg = "${ESC}[100m"
    	    #BrightRed = "${ESC}[91m";     BrightRed_bg = "${ESC}[101m"
    	    #BrightGreen = "${ESC}[92m";   BrightGreen_bg = "${ESC}[102m"
    	    #BrightYellow = "${ESC}[93m";  BrightYellow_bg = "${ESC}[103m"
    	    #BrightBlue = "${ESC}[94m";    BrightBlue_bg = "${ESC}[104m"
    	    #BrightMagenta = "${ESC}[95m"; BrightMagenta_bg = "${ESC}[105m"
    	    #BrightCyan = "${ESC}[96m";    BrightCyan_bg = "${ESC}[106m"
    	    #BrightWhite = "${ESC}[97m";   BrightWhite_bg = "${ESC}[107m"
    	}

    	function Output-Color {
    		param (
    	    		[Parameter(Mandatory=$true)]
    			[string]$Text,
    			[string]$Fg = "",
    			[string]$Bg = ""
    		)
    		return ("{0}{1}{2}{3}" -f $Fg, $Bg, $Text, $RESET_COLOR)
    	}
    #endregion
    
    #region TIMESTAMP
    	$DATE = (Get-Date -Format "HH:mm:ss")
    	$prompt_timestamp = Output-Color -Text $DATE -Fg $ANSIColors.Cyan
    #endregion

    #region DIRECTORY
    	$locations = (Get-Location).Path.Split("\")
        if ($locations.Count -gt 3) {
    	    $amount_between = $locations.Count - 3		# 3 is the amount of locations[?] in $pwd
    	    $pwd = ("{0}\{1}\..{2}..\{3}" -f $locations[0], $locations[1], $amount_between, $locations[-1])
        } else {
            $pwd = Get-Location
        }

    	$prompt_dir =  Output-Color -Text $pwd -Fg $ANSIColors.Yellow
    #endregion

    #region GIT
    	$git_branch_current = git branch --show-current
    	if (!$git_branch_current) {
    	    $git_branch_current = "no git"
    	} else {
            $porcelain = git status --porcelain

            $untracked = ($porcelain | Select-String -Pattern "\?{2}").Count
            $untracked_color = Output-Color -Text $untracked -Fg $ANSIColors.Red

            $added = ($porcelain | Select-String -Pattern "A").Count
            $added_color = Output-Color -Text $added -Fg $ANSIColors.Yellow

            $modified = ($porcelain | Select-String -Pattern "M").Count
            $modified_color = Output-Color -Text ("{0}" -f $modified) -Fg $ANSIColors.Blue

            $prompt_git_status = (" ?{0} +{1} ~{2}" -f $untracked_color, $added_color, $modified_color)
        }

        $prompt_git_branch = Output-Color -Text $git_branch_current
    #endregion

    $prompt_final = ("{0} {1} ({2}{3}) > " -f $prompt_timestamp, $prompt_dir, $prompt_git_branch, $prompt_git_status)

    return $prompt_final
}

function ccd {

    $paths_as_json = Get-Content -Path ("{0}\ccd-paths.json" -f $PSScriptRoot) -Encoding UTF8 | ConvertFrom-Json

	$print = ""
    foreach ($path in $paths_as_json) {
        $nav = ("({0}) {1}" -f $path.Index, $path.Name)
        if ($path.Index -eq 0) {
            $print = $print, $nav -Join ""
        } else {
            $print = $print, $nav -Join "        "
        }
    }

    Write-Output ("AVAILABLE PATHS:`n{0}`n" -f $print)

    $nav_choice = Read-Host "Select by index"

    $indexes = @($paths_as_json | %{$_.Index})
    
    if ($indexes -notcontains $nav_choice) {
        Write-Warning -Message "Rerun and select a correct index!"
        break
    } else {
        $nav_to = $paths_as_json | Where-Object {$_.Index -eq $nav_choice}
        Set-Location $nav_to.Path
    }
}
