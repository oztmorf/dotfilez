# dotfilez

## Linux

### stow

```sh
cd linux
stow -t ~ .
```

## Other

### neovim

#### Lazy

https://github.com/folke/lazy.nvim

Open Lazy interface
```
:Lazy
```

### VSCodium

Path in Windows:
```
%APPDATA%\VSCodium\User\settings.json
```
