syntax on
" Numbers
set number
set relativenumber
" Cursor
set cursorline
"set colorcolumn=100
" Tabs
set expandtab
set tabstop=4
set shiftwidth=4
autocmd FileType python set tabstop=4|set shiftwidth=4|set expandtab
autocmd FileType yaml set tabstop=2|set shiftwidth=2|set expandtab
autocmd FileType typescript set tabstop=2|set shiftwidth=2|set expandtab
" Navigating up and down with 4 rows offset.
set so=4
" Highlight search
set hlsearch
" https://github.com/junegunn/vim-plug
" Plugins will be downloaded under the specified directory.
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

" Declare the list of plugins.
Plug 'gosukiwi/vim-smartpairs'

" List ends here. Plugins become visible to Vim after this call.
call plug#end()
