HISTFILE=~/.histfile
HISTSIZE=7000
SAVEHIST=7000
setopt INC_APPEND_HISTORY
setopt EXTENDED_HISTORY
setopt HIST_IGNORE_SPACE

autoload -Uz promptinit && promptinit
autoload -Uz compinit && compinit
autoload -Uz vcs_info
precmd () { vcs_info }

zstyle disable bzr cdv cvs darcs fossil hg mtn p4 svk svn tla
zstyle enable git
zstyle ':vcs_info:git:*' formats '%b %m'
zstyle ':vcs_info:git*+set-message:*' hooks untracked-git

+vi-untracked-git() {
  porcelain=$(git status --porcelain | awk '{print $1}')
  untracked=$(echo $porcelain | grep '??' | wc -l)
  modified=$(echo $porcelain | grep 'M' | wc -l)
  added=$(echo $porcelain | grep 'A' | wc -l)
  hook_com[misc]="%F{red}?:$untracked %F{yellow}+:$added %F{blue}!:$modified %f"
}

setopt prompt_subst
PS1='
%* %F{yellow}%n%f@%F{cyan}%M%f %~ %F{magenta}${vcs_info_msg_0_}%F{white}
$ '

zstyle :compinstall filename '/home/$USER/.zshrc'
zstyle 'completion:*' select

# KEYS
## To figure out code of key, execute 'cat', press enter, press the key, then Ctrl-C
bindkey -e
bindkey - '^[[H' beginning-of-line
bindkey - '^[[F' end-of-line
bindkey - '^[[3~' delete-char


# ALIAS
if [[ -f /usr/bin/xcolor ]]
then
    alias gc='xcolor'
fi

if [[ -f /usr/bin/nvim ]]
then
	alias vim='nvim'
fi

if [[ -f /usr/bin/ls ]]
then
    alias ls='ls --color -CF'
fi
              
alias myip='curl https://ifconfig.io/ip'
alias alpha='echo {A..Z}'

# ENVIROMENT
## PATH
### Uncomment to add go binary to path
### path+=(/usr/local/go/bin)
### export PATH

## NVM
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

## NODE.JS
## https://nodejs.org/api/cli.html#--max-old-space-sizesize-in-megabytes
export NODE_OPTIONS="--max-old-space-size=4096"

## PULUMI
export PATH=$PATH:$HOME/.pulumi/bin
export PULUMI_CONFIG_PASSPHRASE=dev

## DOTNET
export DOTNET_CLI_TELEMETRY_OPTOUT=1
path+=(~/.dotnet/tools)
export PATH

## EDITOR
### nvim
if [[ -f /usr/bin/nvim ]]
then
	export EDITOR=nvim
fi

### vim
if [[ -f /usr/bin/vim ]]
then
	export EDITOR=vim
fi

